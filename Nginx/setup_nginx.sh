#!/bin/bash

# Mise à jour du système
#yum update -y

# Ajout des dépôts EPEL
#yum install -y epel-release

# Démarrage des vbox invités
#systemctl enable vboxadd
#systemctl enable vboxadd-service

# Clean caches and artifacts
#rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
#yum clean all
#rm -rf /tmp/*
#rm -f /var/log/wtmp /var/log/btmp
#history -c
sudo su -
yum install -y epel-release
yum install -y nginx
#yum install -y vim
#yum install -y python3

systemctl start nginx
systemctl start firewalld

firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
firewall-cmd --reload

systemctl enable nginx

ip a show eth2 | grep inet | awk '{print $2;}' | sed 's/\/.*$//'